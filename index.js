class Live{

  constructor(initData, options = {}) {
    const firstGeneration = this._parseToArray(initData);
    this._generations = [firstGeneration];
    this._yMax = firstGeneration.length;
    this._xMax = firstGeneration[0].length;

    this._markAsAlive = {
        ifAlive: options.ifAlive || [2,3],
        ifDead: options.ifDead || [3],
    }

    this._joinBorders = options.joinBorders || false;
  }

  get currentStep() {
    return this._generations.length-1;
  }
  
  get currentGeneration() {
    return this._generations[this.currentStep];
  }

  set currentGeneration(newGenArr) {
    this._generations.push(newGenArr);
  }

  nextGeneration() {
    const ng = [];
    const cg = this.currentGeneration;

    for(let y = 0; y < cg.length; y++){
        ng[y] = [];
        for(let x = 0; x < (cg[y]).length; x++){
            const isAlive = !!cg[y][x];
            const liveNbs = this._checkNbs(x,y);

            ng[y].push(this._liveOrDead(isAlive, liveNbs));
        }
    }

    this.currentGeneration = ng;
    return ng; 
  }
  
  _parseToArray(str) {
    let rowLengthCheck; //row length checker - for error prevent
    return initData.split('\n').map(val => {
      const arr = [];
      if(!rowLengthCheck) {
          rowLengthCheck = val.length
      } 
      if(rowLengthCheck !== val.length || !val.length) {
          throw 'CRITICAL ERROR! Rows length is different!';
      }
      
      for(let i=0; i<val.length; i++){
          arr.push(parseInt(val[i]));
      }
      return arr;
    });
  }

  _liveOrDead(isAlive, aliveNbs) {
    const needNbs = (isAlive) ? this._markAsAlive.ifAlive : this._markAsAlive.ifDead;

    if(needNbs.indexOf(aliveNbs) >= 0) {
        return 1;
    }

    return 0;
  }
  
  _getNbsXYS(x,y) {
    const cg = this.currentGeneration;
    
    const xys = [];
    for(let xx = x-1, i = 0; i<3; i++, xx = x-1+i) {
        
        if(this._joinBorders) {
            if(xx < 0) {
                xx = this._xMax - 1;
            } else if(xx >= this._xMax) {
                xx = 0;
            }
        } else if (xx < 0 || xx >= this._xMax) {
            continue;
        }

        for(let yy = y-1, j = 0; j<3; j++, yy = y-1+j) {
            if(this._joinBorders) {
                if(yy < 0) {
                    yy = this._yMax - 1;
                } else if(yy >= this._yMax) {
                    yy = 0;
                }
            } else if ( yy < 0 || yy >= this._yMax ) {
                continue;
            }

            if ( xx === x && yy ===y ) {
                continue;
            }

            xys.push({x: xx, y: yy});
        }
    }
    
    return xys;
  }
  
  _checkNbs(x, y) {
    const nbs = this._getNbsXYS(x,y);
    const cg = this.currentGeneration;
    let liveNbs = 0;
    nbs.map(({x,y}) => {
        if(cg[y][x]) {
            liveNbs++;
        }
    });

    return liveNbs;
  }
}

const initData = 
`101100001001
000010001011
010011100000
100100111100
101110000011`;

//classic rules
const live = new Live(initData);

//infinity field (joined borders)
// const live = new Live(initData, {joinBorders: true});

//custom live rules
// const live = new Live(initData, {joinBorders: true, ifAlive: [1,2,3], ifDead: [3,5]});

console.log(live.currentGeneration);
console.log(live.nextGeneration());

//you can use setInterval for loop of generations